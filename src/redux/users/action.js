import {
  IS_SIGNUP,
  LOGGED_USER,
  LOGGED_USER_CHANGE_INFO,
  SET_ERROR_LOGIN,
} from "./constants";

import * as api from "../../components/api/users";

export const setIsSignUp = (bool) => ({ type: IS_SIGNUP, bool });
export const setErrorLogin = (error) => ({ type: SET_ERROR_LOGIN, error });
export const setLoggedUser = (user) => ({ type: LOGGED_USER, user });
export const changeLoggedUserInfo = (data) => ({
  type: LOGGED_USER_CHANGE_INFO,
  data,
});

export const signUp = (data) => async (dispatch) => {
  try {
    const response = await api.signUp(data);
    if (response.ok) {
      dispatch(setIsSignUp(true));
    }
  } catch (err) {
    console.log(err);
  }
};

export const signIn = (attrs) => async (dispatch) => {
  try {
    const response = await api.signIn(attrs.email, attrs.password);
    const data = await response.json();
    if (response.ok && data.length !== 0) {
      dispatch(setLoggedUser(data[0]));
    } else {
      dispatch(setErrorLogin("Invalid Credentials"));
    }
  } catch (err) {
    console.log(err);
  }
};

export const checkUserIsExists = (email) => async (dispatch) => {
  try {
    const response = await api.checkUserIsExists(email);
    const data = await response.json();
    if (response.ok && data.length !== 0) {
      dispatch(setLoggedUser(data[0]));
    }
  } catch (err) {
    console.log(err);
  }
};

export const changeUserInformation = (id, data) => async (dispatch) => {
  try {
    const response = await api.changeUserInformation(id, data);
    if (response.ok) {
      dispatch(changeLoggedUserInfo(data));
    }
  } catch (err) {
    console.log(err);
  }
};

export const deleteUser = (id) => async (dispatch) => {
  try {
    const response = await api.deleteUser(id);
    if (response.ok) {
      localStorage.removeItem("loggedUser");
      dispatch(setLoggedUser({}));
    }
  } catch (err) {
    console.log(err);
  }
};
