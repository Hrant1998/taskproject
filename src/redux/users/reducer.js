import {
  IS_SIGNUP,
  LOGGED_USER,
  LOGGED_USER_CHANGE_INFO,
  SET_ERROR_LOGIN,
} from "./constants";

const initialState = {
  users: [],
  isSignUp: false,
  loggedUser: {},
  errorLogin: "",
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR_LOGIN: {
      return {
        ...state,
        errorLogin: action.error,
      };
    }
    case LOGGED_USER: {
      return {
        ...state,
        loggedUser: action.user,
      };
    }
    case LOGGED_USER_CHANGE_INFO: {
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          ...action.data,
        },
      };
    }
    case IS_SIGNUP: {
      return {
        ...state,
        isSignUp: action.bool,
      };
    }
    default: {
      return state;
    }
  }
};
