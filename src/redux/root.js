import { combineReducers } from "redux";
import users from "./users/reducer";
import article from "./article/reducer";

export default combineReducers({
  users,
  article,
});
