import * as api from "../../components/api/article";

import {
  SET_SEARCH_VALUE,
  SET_ITEMS,
  SET_IS_ADD,
  SET_ONE_ITEM,
  SET_COMMENTS,
  ADD_COMMENT,
} from "./constants";

export const setSearchValue = (data) => ({ type: SET_SEARCH_VALUE, data });
export const setItems = (items) => ({ type: SET_ITEMS, items });
export const setIsAdd = (bool) => ({ type: SET_IS_ADD, bool });
export const addOneComment = (value) => ({ type: ADD_COMMENT, value });
export const setOneItem = (item) => ({ type: SET_ONE_ITEM, item });
export const setComments = (comments) => ({ type: SET_COMMENTS, comments });

export const addArticle = (data) => async (dispatch) => {
  try {
    const response = await api.addArticle(data);
    if (response.ok) {
      dispatch(setIsAdd(true));
    }
  } catch (err) {
    console.log(err);
  }
};

export const getArticles = () => async (dispatch) => {
  try {
    const response = await api.getArticles();
    const data = await response.json();
    if (response.ok) {
      dispatch(setItems(data));
    }
  } catch (err) {
    console.log(err);
  }
};

export const getOneArticle = (id) => async (dispatch) => {
  try {
    const response = await api.getOneArticle(id);
    const data = await response.json();
    if (response.ok) {
      dispatch(setOneItem(data));
    }
  } catch (err) {
    console.log(err);
  }
};

export const addComment = (attrs) => async (dispatch) => {
  try {
    const response = await api.addComment(attrs);
    if (response.ok) {
      dispatch(addOneComment(attrs));
    }
  } catch (err) {
    console.log(err);
  }
};

export const getComments = (id) => async (dispatch) => {
  try {
    const response = await api.getComments(id);
    const data = await response.json();
    if (response.ok) {
      dispatch(setComments(data));
    }
  } catch (err) {
    console.log(err);
  }
};
