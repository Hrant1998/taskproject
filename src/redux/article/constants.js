export const SET_SEARCH_VALUE = "SET_SEARCH_VALUE";
export const SET_ITEMS = "SET_ITEMS";
export const SET_IS_ADD = "SET_IS_ADD";
export const SET_ONE_ITEM = "SET_ONE_ITEM";
export const SET_COMMENTS = "SET_COMMENTS";
export const ADD_COMMENT = "ADD_COMMENT";
