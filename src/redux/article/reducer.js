import {
  SET_SEARCH_VALUE,
  SET_ITEMS,
  SET_IS_ADD,
  SET_ONE_ITEM,
  SET_COMMENTS,
  ADD_COMMENT,
} from "./constants";

const initialState = {
  search: "",
  items: [],
  isAdd: false,
  one_item: {},
  comments: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_COMMENT: {
      return {
        ...state,
        comments: [...state.comments, action.value],
      };
    }
    case SET_COMMENTS: {
      return {
        ...state,
        comments: action.comments,
      };
    }
    case SET_ONE_ITEM: {
      return {
        ...state,
        one_item: action.item,
      };
    }
    case SET_ITEMS: {
      return {
        ...state,
        items: action.items,
      };
    }
    case SET_IS_ADD: {
      return {
        ...state,
        isAdd: action.bool,
      };
    }
    case SET_SEARCH_VALUE: {
      return {
        ...state,
        search: action.data,
      };
    }

    default: {
      return state;
    }
  }
};
