import React, { useState } from "react";
import NavBar from "./view";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { setLoggedUser } from "../../redux/users/action";

const NavBarContainer = (props) => {
  const [isLogOut, setIsLogOut] = useState(false);

  const logOut = () => {
    localStorage.removeItem("loggedUser");
    props.setLoggedUser({});
    setIsLogOut(true);
  };

  return (
    <>
      {isLogOut && <Redirect to="/signin" />}
      <NavBar logOut={logOut} />
    </>
  );
};
NavBarContainer.propTypes = {
  setLoggedUser: PropTypes.func,
};
const mapStateToProps = (state) => {
  return {};
};
export default connect(mapStateToProps, { setLoggedUser })(NavBarContainer);
