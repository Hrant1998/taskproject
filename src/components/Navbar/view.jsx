import React from "react";
import Styles from "./styles.module.css";
import { NavLink } from "react-router-dom";

const NavBar = (props) => {
  return (
    <div className={`${Styles.container} d-flex justify-content-center`}>
      <div className="container d-flex justify-content-between">
        <div className="col-6 d-flex align-items-center">
          <NavLink
            to="/profile"
            className={Styles.nav_item}
            activeClassName={Styles.active_item}
          >
            Profile
          </NavLink>
          <NavLink
            to="/article"
            className={Styles.nav_item}
            activeClassName={Styles.active_item}
          >
            Blog
          </NavLink>
        </div>
        <div className="col-6 d-flex align-items-center justify-content-end">
          <button
            className={`${Styles.logOut} `}
            onClick={() => props.logOut()}
          >
            Log out
          </button>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
