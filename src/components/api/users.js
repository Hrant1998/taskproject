export const signUp = (data) =>
  fetch("http://localhost:3004/users", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-type": "application/json",
    },
  });

export const signIn = (email, password) =>
  fetch(`http://localhost:3004/users?email=${email}&password=${password}`);

export const checkUserIsExists = (email) =>
  fetch(`http://localhost:3004/users?email=${email}`);

export const changeUserInformation = (id, data) =>
  fetch(`http://localhost:3004/users/${id}`, {
    method: "PATCH",
    body: JSON.stringify(data),
    headers: {
      "Content-type": "application/json",
    },
  });

export const deleteUser = (id) =>
  fetch(`http://localhost:3004/users/${id}`, {
    method: "Delete",
  });
