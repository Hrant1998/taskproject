export const addArticle = (data) =>
  fetch("http://localhost:3004/posts", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-type": "application/json",
    },
  });
export const getArticles = () => fetch("http://localhost:3004/posts");
export const getOneArticle = (id) => fetch(`http://localhost:3004/posts/${id}`);

export const addComment = (data) =>
  fetch("http://localhost:3004/comments", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-type": "application/json",
    },
  });

export const getComments = (id) =>
  fetch(`http://localhost:3004/comments?article_id=${id}`);
