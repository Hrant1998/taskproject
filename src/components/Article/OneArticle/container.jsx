import React, { useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import OneArticle from "./view";

import {
  getOneArticle,
  addComment,
  getComments,
} from "../../../redux/article/action";

const OneArticleContainer = (props) => {
  const id = props.match.params.id;

  useEffect(() => {
    props.getOneArticle(id);
    props.getComments(id);
  }, []);
  const add_comment = (e) => {
    const attrs = {
      ...e,
      author_name: props.loggedUser.nickname,
      article_id: props.item.id,
    };
    props.addComment(attrs);
  };

  return (
    <>
      <OneArticle
        item={props.item}
        add_comment={add_comment}
        comments={props.comments.slice(-5)}
      />
    </>
  );
};

OneArticleContainer.propTypes = {
  loggedUser: PropTypes.object,
  item: PropTypes.object,
  comments: PropTypes.array,
  getOneArticle: PropTypes.func,
  addComment: PropTypes.func,
  getComments: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    item: state.article.one_item,
    comments: state.article.comments,
    loggedUser: state.users.loggedUser,
  };
};

export default compose(
  connect(mapStateToProps, { getOneArticle, addComment, getComments }),
  withRouter
)(OneArticleContainer);
