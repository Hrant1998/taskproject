import React from "react";
import Styles from "./styles.module.css";
import blogImage from "../../../static/images/blog.jpg";
import { useFormik } from "formik";
const OneArticle = (props) => {
  const comments = props.comments.map((x) => {
    return (
      <div className={`${Styles.oneComment} d-flex `} key={x.id}>
        <div
          className={`${Styles.commentAuthor} d-flex align-items-center pl-3`}
        >
          {x.author_name}
        </div>
        <div
          className={`${Styles.commentValue}  d-flex align-items-center py-2`}
        >
          {x.comment}
        </div>
      </div>
    );
  });

  const formik = useFormik({
    initialValues: {
      comment: "",
    },
    onSubmit: (e) => {
      props.add_comment(e);
      formik.resetForm();
    },
  });
  return (
    <div className={`${Styles.container} d-flex justify-content-center`}>
      <div className={`${Styles.contnet} container row `}>
        <div className="mt-3 d-flex w-100 d-flex justify-content-between">
          <div className="col-7">
            <div className={Styles.titleContent}>
              Title -<span className={Styles.title}>{props.item.title}</span>
              Category -
              <span className={Styles.title}>{props.item.category}</span>
            </div>
            <div className={Styles.author}>
              Author -
              <span className={Styles.title}>{props.item.author_name}</span>
            </div>
            <div className={Styles.main_text}>{props.item.main_text}</div>
            <div className={Styles.comments}>
              <div className={Styles.commentsHeader}>Comments</div>
              {comments}
            </div>

            <div className={Styles.addComments}>
              <form onSubmit={formik.handleSubmit}>
                <div className="form-group ">
                  <label htmlFor="comment" className="col-form-label">
                    Add Comment
                  </label>
                  <textarea
                    id="comment"
                    className="form-control"
                    name="comment"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.comment}
                  />
                </div>
                <div className="form-group text-right">
                  <button type="submit" className={`${Styles.submit} btn ml-2`}>
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div className="col-5">
            <img src={blogImage} alt="blog image" className={Styles.img} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default OneArticle;
