import React from "react";
import Item from "./view";

const ItemContainer = (props) => {
  const date = new Date(props.created_date);

  const date_now = `${date.getDate()}-${
    date.getMonth() + 1
  }-${date.getFullYear()} ${
    date.getHours() < 10 ? "0" + date.getHours() : date.getHours()
  }:${date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()}`;

  return (
    <>
      <Item
        title={props.title}
        category={props.category}
        created_date={date_now}
        id={props.id}
      />
    </>
  );
};

export default ItemContainer;
