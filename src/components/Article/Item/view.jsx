import React from "react";
import Styles from "./styles.module.css";
import blogImage from "../../../static/images/blog.jpg";

import { NavLink } from "react-router-dom";

const Item = (props) => {
  return (
    <div className={`${Styles.container} d-flex `}>
      <div>
        <img src={blogImage} alt="blog picture" className={Styles.img} />
      </div>
      <div className={`${Styles.seconPart} ml-4 d-flex `}>
        <div className="w-100 pr-4">
          <div className={Styles.names}>
            <div className={Styles.title}>{props.title}</div>
            <div className={Styles.category}>{props.category}</div>
          </div>
          <div className="d-flex justify-content-between w-100">
            <div className="d-flex align-items-center">
              {props.created_date}
            </div>
            <div>
              <NavLink
                to={`/article/${props.id}`}
                className="btn btn-secondary"
              >
                Read More
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Item;
