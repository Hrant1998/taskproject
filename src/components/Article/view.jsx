import React from "react";
import Styles from "./styles.module.css";

import { NavLink } from "react-router-dom";

import Item from "./Item";

const Article = (props) => {
  const posts = props.items.map((x) => <Item key={x.id} {...x} />);
  return (
    <div className={`${Styles.container} d-flex justify-content-center`}>
      <div className={`${Styles.contnet} container row `}>
        <div className="col mt-3 ">
          <div className="d-flex justify-content-between">
            <div className="d-flex">
              <div>
                <input
                  type="text"
                  onChange={(e) => props.setSearchValue(e.target.value)}
                  value={props.search}
                  className="mr-2 "
                  style={{ height: "38px" }}
                  placeholder="Search by title"
                />
              </div>
              <button
                className="btn btn-secondary"
                onClick={() => props.setSortType((prev) => !prev)}
              >
                {" "}
                Sort by date
              </button>
            </div>
            <NavLink className="btn btn-secondary" to="/articles/add">
              Create article
            </NavLink>
          </div>
          <div className={Styles.itemContainer}>{posts}</div>
        </div>
      </div>
    </div>
  );
};

export default Article;
