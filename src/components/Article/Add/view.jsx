import React from "react";
import Styles from "./styles.module.css";
import { useFormik } from "formik";
import * as Yup from "yup";

const AddArticle = (props) => {
  const signinSchema = Yup.object().shape({
    title: Yup.string().required("Required"),
    main_text: Yup.string().required("Required"),
  });
  const formik = useFormik({
    initialValues: {
      title: "",
      category: "Music",
      main_text: "",
    },
    validationSchema: signinSchema,
    onSubmit: (e) => {
      props.add(e);
      formik.resetForm();
    },
  });
  return (
    <div className={`${Styles.container} d-flex justify-content-center`}>
      <div className={`${Styles.contnet} container row `}>
        <div className={`${Styles.form} col`}>
          <form onSubmit={formik.handleSubmit}>
            <div className="form-group">
              <label htmlFor="title" className="col-form-label">
                {formik.errors.title && formik.touched.title ? (
                  <span className={Styles.error}>{formik.errors.title}</span>
                ) : (
                  "Title"
                )}
              </label>
              <input
                id="title"
                type="title"
                className="form-control"
                name="title"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.title}
                style={{
                  borderColor:
                    formik.errors.title && formik.touched.title
                      ? "red"
                      : "rgb(206, 212, 218)",
                }}
              />
            </div>
            <div className="form-group ">
              <label htmlFor="category" className="col-form-label">
                Category
              </label>
              <select
                className="form-control"
                aria-label="Default select example"
                name="category"
                id="category"
                onChange={formik.handleChange}
                value={formik.values.category}
              >
                <option value="Music" defaultValue>
                  Music
                </option>
                <option value="Food">Food</option>
                <option value="Travel">Travel</option>
                <option value="Sports">Sports</option>
                <option value="Fasion">Fasion</option>
                <option value="Fitness">Fitness</option>
              </select>
            </div>
            <div className="form-group ">
              <label htmlFor="main_text" className="col-form-label">
                {formik.errors.main_text && formik.touched.main_text ? (
                  <span className={Styles.error}>
                    {formik.errors.main_text}
                  </span>
                ) : (
                  "Main text"
                )}
              </label>
              <textarea
                id="main_text"
                className="form-control"
                name="main_text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.main_text}
                style={{
                  borderColor:
                    formik.errors.main_text && formik.touched.main_text
                      ? "red"
                      : "rgb(206, 212, 218)",
                }}
              />
            </div>
            <div className="form-group text-right">
              <button type="submit" className={`${Styles.submit} btn ml-2`}>
                Add
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AddArticle;
