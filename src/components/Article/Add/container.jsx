import React from "react";
import { connect } from "react-redux";
import AddArticle from "./view";

import { addArticle, setIsAdd } from "../../../redux/article/action";
import { Redirect } from "react-router-dom";
import { useEffect } from "react";

import PropTypes from "prop-types";

const AddArticleContainer = (props) => {
  useEffect(() => {
    return () => {
      props.setIsAdd(false);
    };
  }, []);

  const add = (e) => {
    const date = new Date();
    const attrs = {
      ...e,
      created_date: date,
      author_name: props.loggedUser.nickname,
      user_id: props.loggedUser.id,
    };
    props.addArticle(attrs);
  };
  return (
    <div>
      {props.isAdd && <Redirect to="/article" />}
      <AddArticle add={add} />
    </div>
  );
};

AddArticleContainer.propTypes = {
  loggedUser: PropTypes.object,
  isAdd: PropTypes.bool,
  addArticle: PropTypes.func,
  setIsAdd: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    loggedUser: state.users.loggedUser,
    isAdd: state.article.isAdd,
  };
};
export default connect(mapStateToProps, { addArticle, setIsAdd })(
  AddArticleContainer
);
