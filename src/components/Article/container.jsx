import React, { useEffect } from "react";
import { connect } from "react-redux";
import Article from "./view";
import PropTypes from "prop-types";

import { setSearchValue, getArticles } from "../../redux/article/action";
import { useState } from "react";

const ArticleContainer = (props) => {
  const [sortType, setSortType] = useState(true);
  const [sortedPosts, setSortedPosts] = useState([]);

  useEffect(() => {
    props.getArticles();
  }, []);

  useEffect(() => {
    setSortedPosts(
      props.items.slice().sort((a, b) => {
        if (a.created_date > b.created_date) return sortType ? -1 : 1;
        if (a.created_date < b.created_date) return sortType ? 1 : -1;
        return 0;
      })
    );
  }, [props.items, sortType]);

  return (
    <>
      <Article
        user={props.loggedUser}
        search={props.search}
        setSearchValue={props.setSearchValue}
        setSortType={setSortType}
        sortType={sortType}
        items={
          !!props.search
            ? sortedPosts.filter((x) =>
                x.title.toUpperCase().includes(props.search.toUpperCase())
              )
            : sortedPosts
        }
      />
    </>
  );
};

ArticleContainer.propTypes = {
  loggedUser: PropTypes.object,
  search: PropTypes.string,
  items: PropTypes.array,
  setSearchValue: PropTypes.func,
  getArticles: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    loggedUser: state.users.loggedUser,
    search: state.article.search,
    items: state.article.items,
  };
};

export default connect(mapStateToProps, { setSearchValue, getArticles })(
  ArticleContainer
);
