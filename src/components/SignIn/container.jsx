import React from "react";
import { connect } from "react-redux";
import SignIn from "./view";

import { signIn } from "../../redux/users/action";
import { Redirect } from "react-router-dom";
import { useEffect } from "react";

import PropTypes from "prop-types";
const SignInContainer = (props) => {
  useEffect(() => {
    if (Object.keys(props.loggedUser).length !== 0) {
      localStorage.setItem(
        "loggedUser",
        JSON.stringify({ email: props.loggedUser.email })
      );
    }
  }, [props.loggedUser]);

  const login = (attrs) => {
    props.signIn(attrs);
  };

  return (
    <>
      {Object.keys(props.loggedUser).length !== 0 && <Redirect to="/profile" />}
      <SignIn login={login} errorLogin={props.errorLogin} />
    </>
  );
};

SignInContainer.propTypes = {
  loggedUser: PropTypes.object,
  errorLogin: PropTypes.string,
  signIn: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    loggedUser: state.users.loggedUser,
    errorLogin: state.users.errorLogin,
  };
};
export default connect(mapStateToProps, { signIn })(SignInContainer);
