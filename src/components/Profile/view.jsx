import React from "react";
import Styles from "./styles.module.css";
import userIcon from "../../static/images/userIcon.png";
import * as Yup from "yup";
import { useFormik } from "formik";

const Profile = (props) => {
  const signinSchema = Yup.object().shape({
    nickname: Yup.string(),
    password: Yup.string().min(6, "Too Short!").max(20, "Too Long!"),
    confirm_password: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });
  const formik = useFormik({
    initialValues: {
      nickname: "",
      password: "",
      confirm_password: "",
    },
    validationSchema: signinSchema,
    onSubmit: (e) => {
      props.change(e);
      formik.resetForm();
    },
  });
  return (
    <div className={`${Styles.container} d-flex justify-content-center`}>
      <div className={`${Styles.contnet} container row `}>
        <div className="col-md-4 ">
          <div className="row mt-5">
            <div className="col">
              <div className="row">
                <div className="col">
                  <img src={userIcon} alt="userIcon" className={Styles.img} />
                </div>
              </div>
              <div className="row my-3">
                <div className="col">Email: {props.user.email}</div>
              </div>
              <div className="row">
                <div className="col text-start">
                  Nickname: {props.user.nickname}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-8">
          <div className="row mt-5">
            <div className="col">
              <div className="row">
                <div className="col">Change information</div>
              </div>
              <div className="row my-3">
                <div className={`${Styles.form} col`}>
                  <form onSubmit={formik.handleSubmit}>
                    <div className="form-group">
                      <label htmlFor="nickname" className="col-form-label">
                        {formik.errors.nickname && formik.touched.nickname ? (
                          <span className={Styles.error}>
                            {formik.errors.nickname}
                          </span>
                        ) : (
                          "Nickname"
                        )}
                      </label>
                      <input
                        id="nickname"
                        type="nickname"
                        className="form-control"
                        name="nickname"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.nickname}
                        style={{
                          borderColor:
                            formik.errors.nickname && formik.touched.nickname
                              ? "red"
                              : "rgb(206, 212, 218)",
                        }}
                      />
                    </div>
                    <div className="form-group mb-4">
                      <label htmlFor="password" className="col-form-label">
                        {formik.errors.password && formik.touched.password ? (
                          <span className={Styles.error}>
                            {formik.errors.password}
                          </span>
                        ) : (
                          "Password"
                        )}
                      </label>
                      <input
                        id="password"
                        type="password"
                        className="form-control"
                        name="password"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.password}
                        style={{
                          borderColor:
                            formik.errors.password && formik.touched.password
                              ? "red"
                              : "rgb(206, 212, 218)",
                        }}
                      />
                    </div>
                    <div className="form-group mb-4">
                      <label htmlFor="password" className="col-form-label">
                        {formik.errors.confirm_password &&
                        formik.touched.confirm_password ? (
                          <span className={Styles.error}>
                            {formik.errors.confirm_password}
                          </span>
                        ) : (
                          "Confirm Password"
                        )}
                      </label>
                      <input
                        id="confirm_password"
                        type="password"
                        className="form-control"
                        name="confirm_password"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.confirm_password}
                        style={{
                          borderColor:
                            formik.errors.confirm_password &&
                            formik.touched.confirm_password
                              ? "red"
                              : "rgb(206, 212, 218)",
                        }}
                      />
                    </div>
                    <div className="form-group text-right">
                      <button
                        type="button"
                        onClick={() => props.deleteUser()}
                        className="btn btn-danger "
                      >
                        Delete User
                      </button>
                      <button
                        type="submit"
                        className={`${Styles.submit} btn ml-2`}
                      >
                        Change
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
