import React from "react";
import { connect } from "react-redux";
import Profile from "./view";
import PropTypes from "prop-types";
import { changeUserInformation, deleteUser } from "../../redux/users/action";

const ProfileContainer = (props) => {
  const change = (data) => {
    const attrs = {
      nickname: data.nickname || props.loggedUser.nickname,
      password: data.password || props.loggedUser.password,
    };
    props.changeUserInformation(props.loggedUser.id, attrs);
  };

  const deleteUser = () => {
    props.deleteUser(props.loggedUser.id);
  };
  return (
    <>
      <Profile
        user={props.loggedUser}
        change={change}
        deleteUser={deleteUser}
      />
    </>
  );
};
ProfileContainer.propTypes = {
  loggedUser: PropTypes.object,
  deleteUser: PropTypes.func,
  changeUserInformation: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    loggedUser: state.users.loggedUser,
  };
};

export default connect(mapStateToProps, { changeUserInformation, deleteUser })(
  ProfileContainer
);
