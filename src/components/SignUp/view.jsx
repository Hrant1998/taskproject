import React from "react";
import Styles from "./styles.module.css";
import { NavLink } from "react-router-dom";
import * as Yup from "yup";

import { useFormik } from "formik";

const SignUp = (props) => {
  const signinSchema = Yup.object().shape({
    email: Yup.string().email("Invalid email").required("Email is required"),
    nickname: Yup.string().required("Nickname is required"),
    password: Yup.string()
      .min(6, "Too Short!")
      .max(20, "Too Long!")
      .required("Password is required"),
    confirmPassword: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });
  const formik = useFormik({
    initialValues: {
      email: "",
      nickname: "",
      password: "",
      confirmPassword: "",
    },
    validationSchema: signinSchema,
    onSubmit: (e) => {
      props.add_user(e);
      formik.resetForm();
    },
  });
  return (
    <div className="container">
      <div
        className={`${Styles.container} d-flex justify-content-center align-items-center`}
      >
        <div className={Styles.content}>
          <div className={`${Styles.nav} d-flex `}>
            <NavLink
              className={`${Styles.btn} d-flex align-items-center justify-content-center`}
              to="/signin"
              activeClassName={Styles.active_btn}
            >
              Sign In
            </NavLink>
            <NavLink
              className={`${Styles.btn} d-flex align-items-center justify-content-center`}
              to="/signup"
              activeClassName={Styles.active_btn}
            >
              Sign Up
            </NavLink>
          </div>
          <div>
            <div
              className={`${Styles.header} d-flex align-items-center justify-content-center`}
            >
              <h2>Sign up</h2>
            </div>
            <div className="d-flex justify-content-center">
              <div className={`${Styles.form}`}>
                <form onSubmit={formik.handleSubmit}>
                  <div className="form-group">
                    <label htmlFor="email" className="col-form-label">
                      {formik.errors.email && formik.touched.email ? (
                        <span className={Styles.error}>
                          {formik.errors.email}
                        </span>
                      ) : (
                        "Email"
                      )}
                    </label>
                    <input
                      id="email"
                      type="email"
                      className="form-control"
                      name="email"
                      autoFocus={true}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.email}
                      style={{
                        borderColor:
                          formik.errors.email && formik.touched.email
                            ? "red"
                            : "rgb(206, 212, 218)",
                      }}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="nickname" className="col-form-label">
                      {formik.errors.nickname && formik.touched.nickname ? (
                        <span className={Styles.error}>
                          {formik.errors.nickname}
                        </span>
                      ) : (
                        "Nickname"
                      )}
                    </label>
                    <input
                      id="nickname"
                      type="text"
                      className="form-control"
                      name="nickname"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.nickname}
                      style={{
                        borderColor:
                          formik.errors.nickname && formik.touched.nickname
                            ? "red"
                            : "rgb(206, 212, 218)",
                      }}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="password" className="col-form-label">
                      {formik.errors.password && formik.touched.password ? (
                        <span className={Styles.error}>
                          {formik.errors.password}
                        </span>
                      ) : (
                        "Password"
                      )}
                    </label>
                    <input
                      id="password"
                      type="password"
                      className="form-control"
                      name="password"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.password}
                      style={{
                        borderColor:
                          formik.errors.password && formik.touched.password
                            ? "red"
                            : "rgb(206, 212, 218)",
                      }}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="confirmPassword" className="col-form-label">
                      {formik.errors.confirmPassword &&
                      formik.touched.confirmPassword ? (
                        <span className={Styles.error}>
                          {formik.errors.confirmPassword}
                        </span>
                      ) : (
                        "Confirm Password"
                      )}
                    </label>
                    <input
                      id="confirmPassword"
                      type="password"
                      className="form-control"
                      name="confirmPassword"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.confirmPassword}
                      style={{
                        borderColor:
                          formik.errors.confirmPassword &&
                          formik.touched.confirmPassword
                            ? "red"
                            : "rgb(206, 212, 218)",
                      }}
                    />
                  </div>
                  <div className="form-group text-right">
                    <button type="submit" className={`${Styles.submit} btn`}>
                      Sign Up
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
