import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import SignUp from "./view";

import { signUp, setIsSignUp } from "../../redux/users/action";

const SignUpContainer = (props) => {
  useEffect(() => {
    return () => {
      props.setIsSignUp(false);
    };
  }, []);

  const add_user = (e) => {
    const data = {
      email: e.email,
      nickname: e.nickname,
      password: e.password,
    };
    props.signUp(data);
  };

  return (
    <>
      {props.isSignUp && <Redirect to="/signin" />}
      <SignUp add_user={add_user} />
    </>
  );
};

SignUpContainer.propTypes = {
  isSignUp: PropTypes.bool,

  signUp: PropTypes.func,
  setIsSignUp: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    isSignUp: state.users.isSignUp,
  };
};
export default connect(mapStateToProps, { signUp, setIsSignUp })(
  SignUpContainer
);
