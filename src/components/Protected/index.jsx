import React from "react";
import { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import NavBar from "../Navbar";
import Profile from "../Profile";
import Article from "../Article";
import AddArticle from "../Article/Add";
import OneArticle from "../Article/OneArticle";
import PropTypes from "prop-types";

import { checkUserIsExists } from "../../redux/users/action";

const ProtectedRoute = (props) => {
  const userFromLocalStorage = JSON.parse(localStorage.getItem("loggedUser"));

  useEffect(() => {
    if (!!userFromLocalStorage && Object.keys(props.loggedUser).length === 0) {
      props.checkUserIsExists(userFromLocalStorage.email);
    }
  }, []);

  return (
    <div>
      {!userFromLocalStorage && <Redirect to="/signin" />}
      <NavBar />
      <Switch>
        <Route exact path="/profile" render={() => <Profile />} />
        <Route exact path="/articles/add" render={() => <AddArticle />} />
        <Route exact path="/article/:id" render={() => <OneArticle />} />
        <Route exact path="/article" render={() => <Article />} />
        <Route
          render={() => (
            <div className="d-flex justify-content-center">
              <h2>404 Page Not Found</h2>
            </div>
          )}
        />
      </Switch>
    </div>
  );
};

ProtectedRoute.propTypes = {
  loggedUser: PropTypes.object,
  checkUserIsExists: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    loggedUser: state.users.loggedUser,
  };
};
export default connect(mapStateToProps, { checkUserIsExists })(ProtectedRoute);
