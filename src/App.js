import "./App.css";
import { Route, Switch } from "react-router-dom";

import SignUp from "./components/SignUp";
import SignIn from "./components/SignIn";
import Profile from "./components/Profile";
import ProtectedRoute from "./components/Protected";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/signup" render={() => <SignUp />} />
        <Route exact path="/signin" render={() => <SignIn />} />
        <ProtectedRoute component={Profile} />
        <Route render={() => <div>404 Page Not Found</div>} />
      </Switch>
    </div>
  );
}

export default App;
